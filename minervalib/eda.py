import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import sys
from pylab import rcParams
from statsmodels.tsa.stattools import pacf, acf
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.stattools import adfuller
import statsmodels.api as sm

def correlations_long_format(df: pd.DataFrame) -> pd.DataFrame:
  """Function that returns correlations between every pair of different
  columns, where each row shows a different correlation in the form
  {series_1, series_2, correlation}
  Args:
    df: A pandas dataframe of numeric values only 
  Returns:
    corr_long: A pandas dataframe with two string columns and a numeric column
  """
  dfcorr = df.corr()
  # Avoiding conflicts in case the column series_2 already exists
  cols = [col+'_' for col in dfcorr.columns]
  dfcorr.columns = cols
  # Adding a column containing respective column names
  dfcorr['series_2'] = dfcorr.columns
  # Unpivoting dataframe
  corr_long = pd.melt(dfcorr, id_vars='series_2', value_vars=cols, var_name='series_1', value_name='correlation')
  # Removes the _ at the end of string
  corr_long['series_1'] = corr_long.series_1.str[:-1]
  corr_long['series_2'] = corr_long.series_2.str[:-1]
  # Removing unnecessary rows
  corr_long = corr_long[(corr_long.series_1 < corr_long.series_2)]
  # Reassigning the order of the columns
  corr_long = corr_long[['series_1', 'series_2', 'correlation']]
  return corr_long

def plot_two_series(series_1 : pd.Series, series_2 : pd.Series, figsize = (16, 8)):
  """A function that plots two series and also shows the correlation between them
  Args:
    series_1: Series in blue; the index must be in format datetime.
    series_2: Series in red; the index must be in format datetime.
    figsize: Size of the picture (in inches) as a tuple 
  Returns:
    nothing
  """
  
  rcParams['figure.figsize'] = figsize
  fig, ax1 = plt.subplots()
  color = 'tab:red'
  ax1.set_xlabel('DATE')
  ax1.set_title(series_1.name + ' x ' + series_2.name)
  ax1.set_ylabel(series_2.name, color = color)
  ax1.plot(series_2, color = color)
  textstr = '\n'.join((r'$\rho=%.2f$' % (series_2.corr(series_1).round(2)), ))
  props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
  ax1.text(0.05, 0.95, textstr, transform=ax1.transAxes, fontsize=8, verticalalignment='top', bbox=props)
  ax1.tick_params(axis= 'x', labelrotation = 45, labelsize = "small")
  ax1.tick_params(axis= 'y', labelcolor = color, labelrotation = 90)
  ax2 = ax1.twinx()
  color = 'tab:blue'
  ax2.set_ylabel(series_1.name, color = color)
  ax2.plot(series_1, color = color)
  ax2.tick_params(axis= 'y', labelcolor = color)
  plt.show()
