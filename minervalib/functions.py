import pandas as pd


def hello() -> str:
    """
    A simple hello function

    Returns:
        String "Hello"
    """
    return "hello"


def compare_dataframe(df_first: pd.DataFrame, df_second: pd.DataFrame,
                      id_df_first: str = 'first', id_df_second: str = 'second') -> pd.DataFrame:
    """
    Compare columns and elements non-NaN of two data frames rounded in 4, and return
    the uniques portions of two data frames in pandas data frame structure.
    Args:
        df_first: first dataframe
        df_second: second dataframe
        id_df_first: name to identify the first dataframe
        id_df_second: name to identify the second dataframe
    Returns:
        DataFrame with the difference between df_first and df_second
    Side Effect:
        A new column “where” is added to identify every line origin
    """
    df_first.reset_index(drop=True, inplace=True)
    df_second.reset_index(drop=True, inplace=True)

    # Add new column to identify the dfs
    df_first.loc[:, 'where'] = id_df_first
    df_second.loc[:, 'where'] = id_df_second

    # Avoid comparison of Nan and with more than 4 decimals
    df_first = df_first.fillna(0).round(4)
    df_second = df_second.fillna(0).round(4)
    df_full = pd.concat([df_first, df_second], sort=False)

    return df_full.drop_duplicates(subset=set(df_full.columns) - set(['where']), keep=False)
