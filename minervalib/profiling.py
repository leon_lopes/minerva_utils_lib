import pandas as pd


def formatDataFrame(df: pd.DataFrame) -> pd.DataFrame:
    """
    Format master table dataframe

    Args:
      df: master table dataframe

    Returns:
      Returns the dataframe with index and type alterations.
    """
    try:
        df = df.to_frame()
    except:
        print("It is already a dataframe-type object")

    df["ref_date"] = df["ref_date"].apply(pd.to_datetime)
    df = df.sort_values("ref_date")
    df = df.select_dtypes(exclude=['object'])
    df["year"] = df["ref_date"].dt.year
    df["year"] = df["year"].astype(str)
    return df


def splitByYear(df: pd.DataFrame, year: str) -> pd.DataFrame:
    """
    Split the dataframe by year

    Args:
      df: dataframe to be split.
      year: year used to filter.

    Returns:
      Returns the dataframe splited by year.
    """
    df_splited = df[df["year"] == year]
    return df_splited.drop(["year", "ref_date"], axis=1)


def generateProfilingByYear(df: pd.DataFrame) -> pd.DataFrame:
    """
    Generate profiling dataframe separated by year

    Args:
      df: dataframe that will generate the profiling.

    Returns:
      Returns the dataframe with profiling.
    """
    df_profiling = pd.DataFrame(
        columns=["Ano", "Coluna", "Nulo", "Desvio_Padrao", "Simetria", "Coef_Var"]
    )
    anos = df["year"].unique()
    
    for ano in anos:
        df_aux = splitByYear(df, ano)

        for colunm in df_aux:
            nulls = df_aux[colunm].isna().sum()
            desvio_padrao = df_aux[colunm].std()
            simetria = df_aux[colunm].skew()
            amplitude = df_aux[colunm].max() - df_aux[colunm].min()
            
            if desvio_padrao == 0 or df_aux[colunm].mean() == 0:
              coef_var = 0
            else:
              coef_var = df_aux[colunm].std()/df_aux[colunm].mean() * 100
            
            
            
            valores = {
                "Ano": ano,
                "Coluna": colunm,
                "Nulo": nulls,
                "Desvio_Padrao": desvio_padrao,
                "Simetria": simetria,
                "Amplitude": amplitude,                
                "Coef_Var": coef_var                
            }

            df_profiling = df_profiling.append(valores, ignore_index=True)
            
    return df_profiling

def generateProfiling(df: pd.DataFrame) -> pd.DataFrame:
    """
    Generate profiling dataframe

    Args:
      df: dataframe that will generate the profiling.

    Returns:
      Returns the dataframe with profiling.
    """
    df_profiling = pd.DataFrame(
        columns=["Coluna", "Nulo", "Desvio_Padrao", "Simetria", "Coef_Var"]
    )
    df_aux = df.drop(["year", "ref_date"], axis=1)
    
    for colunm in df_aux:
        nulls = df_aux[colunm].isna().sum()
        desvio_padrao = df_aux[colunm].std()
        simetria = df_aux[colunm].skew()
        amplitude = df_aux[colunm].max() - df_aux[colunm].min()

        if desvio_padrao == 0 or df_aux[colunm].mean() == 0:
            coef_var = 0
        else:
            coef_var = df_aux[colunm].std() / df_aux[colunm].mean() * 100
        valores = {
            "Coluna": colunm,
            "Nulo": nulls,
            "Desvio_Padrao": desvio_padrao,
            "Simetria": simetria,
            "Amplitude": amplitude,
            "Coef_Var": coef_var,
        }

        df_profiling = df_profiling.append(valores, ignore_index=True)
    return df_profiling



def duplicateValuesByYear(df: pd.DataFrame) -> pd.DataFrame:
    """
    Count duplicated values by year
    """
    df_duplicates = pd.DataFrame(columns=["Year", "Column", "Value", "Quantity"])
    anos = df["year"].unique()

    for ano in anos:
        df_aux = splitByYear(df, ano)

        for column in df_aux:
            dici = df_aux[column].value_counts()

            for index, value in dici.items():
                valores = {
                    "Year": ano,
                    "Column": column,
                    "Value": index,
                    "Quantity": value,
                }
                df_duplicates = df_duplicates.append(valores, ignore_index=True)
    return df_duplicates[df_duplicates["Quantity"] > 1]

def duplicateValues(df: pd.DataFrame) -> pd.DataFrame:
    """
    Count duplicated values
    """
    df_duplicates = pd.DataFrame(columns=["Column", "Value", "Quantity"])
    for column in df:
        dici = df[column].value_counts()

        for index, value in dici.items():
            valores = {
                "Column": column,
                "Value": index,
                "Quantity": value,
            }
            df_duplicates = df_duplicates.append(valores, ignore_index=True)
    return df_duplicates[df_duplicates["Quantity"] > 1]    