from setuptools import find_packages, setup

setup(
    name='minervalib',
    packages=find_packages(include=['minervalib']),
    version='0.1.0',
    description='Utility library for the Advanced Analytics team',
    author='Leonardo Lopes Nunes',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',)
